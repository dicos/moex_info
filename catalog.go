package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"path/filepath"
	"log"
	"sort"
)

type FInfo struct {
	path string
	FileInfo os.FileInfo
}

type FInfoSlice []FInfo

func (slice FInfoSlice) Len() int {
	return len(slice)
}

func (slice FInfoSlice) Less(i, j int) bool {
	return slice[i].FileInfo.ModTime().Before(slice[j].FileInfo.ModTime())
}
func (slice FInfoSlice) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

func getListFiles(paths []string) []string {
	count_paths := len(paths)
	info := make(FInfoSlice, count_paths)
	for index, path := range paths {
		info[index].FileInfo, _ = os.Stat(path)
		info[index].path = path
	}
	sort.Sort(info)
	new_paths := paths
	for index, f_struct := range info {
		new_paths[index] = f_struct.path
	}
	return new_paths
}

type csvSlice [][]string

func (c csvSlice) Len() int {
	return len(c)
}
func (c csvSlice) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}
func (c csvSlice) Less(i, j int) bool {
	return c[i][4] < c[j][4]
}

func readCsv(path string) [][]string {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	reader := csv.NewReader(file)
	reader.Comma = ';'
	values, _ := reader.ReadAll()
	sort_values := values[1:]
	sort.Sort(csvSlice(sort_values))
	return sort_values
}

func in(name string, info map[string]*csv.Writer) bool {
	_, ok := info[name]
	return ok
}

func writeCsvFile(path string) {
	info := make(map[string]*csv.Writer)
	for _, data := range readCsv(path) {
		if len(data) == 1 {
			break
		}
		name := data[1]
		if !in(name, info) {
			new_path := fmt.Sprintf("%s/%s.csv", os.Args[2], name)
			w_file, err := os.OpenFile(new_path, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666)
			if err != nil {
				log.Fatal(err)
			}
			defer w_file.Close()
			info[name] = csv.NewWriter(w_file)
			defer info[name].Flush()
		}
		err := info[name].Write(data[2:5])
		if err != nil {
			log.Panic(err)
		}
	}
}

func main() {
	pattern_out := fmt.Sprintf("%s/*.csv", os.Args[2])
	old_files, _ := filepath.Glob(pattern_out)
	for _, path := range old_files {
		os.Remove(path)
	}

	pattern_in := fmt.Sprintf("%s/*.csv", os.Args[1])
	paths, err := filepath.Glob(pattern_in)
	if err != nil {
		log.Fatal(err)
	}
	f_info := getListFiles(paths)
	for _, path := range f_info {
		writeCsvFile(path)
	}
}
