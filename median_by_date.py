#coding: utf8
import csv
import os
import sys
from statistics import median, mean

info_file_path = sys.argv[1]
info_file = open(info_file_path, 'r')
info_csv = csv.reader(info_file, delimiter=";")

data_filename = 'price-%s' % os.path.basename(info_file_path)
data_file = open(data_filename, 'w')
data_csv = csv.writer(data_file, delimiter=";")

day = ''
prices_day = []
data_csv.writerow(("День", "средняя цена", "медиана цены",
                   "Максимальное отклонение от медианы цены вверх",
                   "Минимальное отклонение от медианы цены вверх",
                   "Медиана отклонения от медианы цены вверх",
                   "Максимальное отклонение от средней цены вверх",
                   "Минимальное отклонение от средней цены вверх",
                   "Среднее отклонение от средней цены вверх",
                   "Максимальное отклонение от медианы цены вниз",
                   "Минимальное отклонение от медианы цены вних",
                   "Медиана отклонения от медианы цены вниз",
                   "Максимальное отклонение от средней цены вниз",
                   "Минимальное отклонение от средней цены вниз",
                   "Среднее отклонение от средней цены вниз",))
for row in info_csv:
    curr_date = row[2][:10]
    if day != curr_date and prices_day:
        if len(prices_day) > 100:
            prices_median = median(prices_day)
            prices_mean = mean(prices_day)
            up_median = tuple((p for p in prices_day if p > prices_median))
            if up_median:
                max_up_median = max(up_median) - prices_median
                min_up_median = min(up_median) - prices_median
                median_up = median(up_median) - prices_median
            else:
                max_up_median = min_up_median = median_up = 0
            up_mean = tuple((p for p in prices_day if p > prices_mean))
            if up_mean:
                max_up_mean = max(up_mean) - prices_mean
                min_up_mean = min(up_mean) - prices_mean
                mean_up = mean(up_mean) - prices_mean
            else:
                max_up_mean = min_up_mean = mean_up = 0
            down_median = tuple((p for p in prices_day if p < prices_median))
            if down_median:
                max_down_median = prices_median - min(down_median)
                min_down_median = prices_median - max(down_median)
                median_down = prices_median - mean(down_median)
            else:
                max_down_median = min_down_median = median_down = 0
            down_mean = tuple((p for p in prices_day if p < prices_mean))
            if down_mean:
                max_down_mean = prices_mean - min(down_mean)
                min_down_mean = prices_mean - min(down_mean)
                mean_down = prices_mean - mean(down_mean)
            else:
                max_down_mean = min_down_mean = mean_down = 0
            data_row = (day, prices_mean, prices_median, max_up_median,
                        min_up_median, median_up, max_up_mean, min_up_mean,
                        mean_up, max_down_median, min_down_median,
                        median_down, max_down_mean, min_down_mean, mean_down)
            data_csv.writerow(data_row)
        day = curr_date
        prices_day = []
    prices_day.append(float(row[0]))

