#coding: utf8
import csv
import glob
import os
import sys

search_dir = sys.argv[1]
# remove anything from the list that is not a file (directories, symlinks)
# thanks to J.F. Sebastion for pointing out that the requirement was a list 
# of files (presumably not including directories)  
files = filter(os.path.isfile, glob.glob(search_dir + "*.csv"))
files = sorted(files, key=lambda x: os.path.getmtime(x))
files = (open(f) for f in files)

read_csv = (csv.reader(open(f), delimiter=";") for f in files)

path = sys.argv[2] + '%s.csv'

for file_read in files:
    info = {}
    csv_files = {}
    read = csv.reader(file_read, delimiter=";")
    read = tuple(read)[1:]
    read = sorted(read, key=lambda x: x[4], reverse=False)
    for data in read:
        name = data[1]
        if name not in info:
            filename = path % name
            csv_files[name] = open(filename, 'a')
            info[name] = csv.writer(csv_files[name], delimiter=";")
        info[name].writerow(data[2:5])
    [f.close() for f in csv_files.values()]
    file_read.close()
    del csv_files
    del info
